namespace SB_Remote_WakeUp
{
    partial class FrmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmMain));
            this.kryptonManager1 = new ComponentFactory.Krypton.Toolkit.KryptonManager(this.components);
            this.FrmMainTabs = new System.Windows.Forms.TabControl();
            this.tab_QuickWakeUp = new System.Windows.Forms.TabPage();
            this.panel_QuickWakeUp = new ComponentFactory.Krypton.Toolkit.KryptonHeaderGroup();
            this.QuickWakeUp_DO_btn = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.tab_StoredWakeUp = new System.Windows.Forms.TabPage();
            this.tabSettings = new System.Windows.Forms.TabPage();
            this.quick_WakeUp_MAC = new TestFlexFieldControl.MACAddressControl();
            this.quick_IP2MAC = new TestFlexFieldControl.IPAddressControl();
            this.quick_IP2MAC_btn = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.FrmMainTabs.SuspendLayout();
            this.tab_QuickWakeUp.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panel_QuickWakeUp)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panel_QuickWakeUp.Panel)).BeginInit();
            this.panel_QuickWakeUp.Panel.SuspendLayout();
            this.panel_QuickWakeUp.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.QuickWakeUp_DO_btn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.quick_IP2MAC_btn)).BeginInit();
            this.SuspendLayout();
            // 
            // kryptonManager1
            // 
            this.kryptonManager1.GlobalPaletteMode = ComponentFactory.Krypton.Toolkit.PaletteModeManager.Office2007Black;
            // 
            // FrmMainTabs
            // 
            this.FrmMainTabs.Alignment = System.Windows.Forms.TabAlignment.Bottom;
            this.FrmMainTabs.Controls.Add(this.tab_QuickWakeUp);
            this.FrmMainTabs.Controls.Add(this.tab_StoredWakeUp);
            this.FrmMainTabs.Controls.Add(this.tabSettings);
            this.FrmMainTabs.Dock = System.Windows.Forms.DockStyle.Fill;
            this.FrmMainTabs.Location = new System.Drawing.Point(0, 0);
            this.FrmMainTabs.Name = "FrmMainTabs";
            this.FrmMainTabs.SelectedIndex = 0;
            this.FrmMainTabs.Size = new System.Drawing.Size(638, 356);
            this.FrmMainTabs.TabIndex = 0;
            // 
            // tab_QuickWakeUp
            // 
            this.tab_QuickWakeUp.Controls.Add(this.panel_QuickWakeUp);
            this.tab_QuickWakeUp.Location = new System.Drawing.Point(4, 4);
            this.tab_QuickWakeUp.Name = "tab_QuickWakeUp";
            this.tab_QuickWakeUp.Padding = new System.Windows.Forms.Padding(3);
            this.tab_QuickWakeUp.Size = new System.Drawing.Size(630, 330);
            this.tab_QuickWakeUp.TabIndex = 0;
            this.tab_QuickWakeUp.Text = "Quick WakeUp";
            this.tab_QuickWakeUp.UseVisualStyleBackColor = true;
            // 
            // panel_QuickWakeUp
            // 
            this.panel_QuickWakeUp.DirtyPaletteCounter = 9;
            this.panel_QuickWakeUp.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel_QuickWakeUp.Location = new System.Drawing.Point(3, 3);
            this.panel_QuickWakeUp.Name = "panel_QuickWakeUp";
            // 
            // panel_QuickWakeUp.Panel
            // 
            this.panel_QuickWakeUp.Panel.Controls.Add(this.quick_IP2MAC_btn);
            this.panel_QuickWakeUp.Panel.Controls.Add(this.quick_IP2MAC);
            this.panel_QuickWakeUp.Panel.Controls.Add(this.quick_WakeUp_MAC);
            this.panel_QuickWakeUp.Panel.Controls.Add(this.QuickWakeUp_DO_btn);
            this.panel_QuickWakeUp.Size = new System.Drawing.Size(624, 324);
            this.panel_QuickWakeUp.TabIndex = 0;
            this.panel_QuickWakeUp.Text = "Quick WakeUp remote computer";
            this.panel_QuickWakeUp.ValuesPrimary.Description = "";
            this.panel_QuickWakeUp.ValuesPrimary.Heading = "Quick WakeUp remote computer";
            this.panel_QuickWakeUp.ValuesPrimary.Image = ((System.Drawing.Image)(resources.GetObject("panel_QuickWakeUp.ValuesPrimary.Image")));
            this.panel_QuickWakeUp.ValuesSecondary.Description = "";
            this.panel_QuickWakeUp.ValuesSecondary.Heading = "Use this tab to wake remote computer using MAC address";
            this.panel_QuickWakeUp.ValuesSecondary.Image = null;
            // 
            // QuickWakeUp_DO_btn
            // 
            this.QuickWakeUp_DO_btn.DirtyPaletteCounter = 10;
            this.QuickWakeUp_DO_btn.Location = new System.Drawing.Point(314, 124);
            this.QuickWakeUp_DO_btn.Name = "QuickWakeUp_DO_btn";
            this.QuickWakeUp_DO_btn.Size = new System.Drawing.Size(170, 23);
            this.QuickWakeUp_DO_btn.TabIndex = 1;
            this.QuickWakeUp_DO_btn.Text = "WakeUp that computer";
            this.QuickWakeUp_DO_btn.Values.ExtraText = "";
            this.QuickWakeUp_DO_btn.Values.Image = null;
            this.QuickWakeUp_DO_btn.Values.ImageStates.ImageCheckedNormal = null;
            this.QuickWakeUp_DO_btn.Values.ImageStates.ImageCheckedPressed = null;
            this.QuickWakeUp_DO_btn.Values.ImageStates.ImageCheckedTracking = null;
            this.QuickWakeUp_DO_btn.Values.Text = "WakeUp that computer";
            this.QuickWakeUp_DO_btn.Click += new System.EventHandler(this.QuickWakeUp_DO_btn_Click);
            // 
            // tab_StoredWakeUp
            // 
            this.tab_StoredWakeUp.Location = new System.Drawing.Point(4, 4);
            this.tab_StoredWakeUp.Name = "tab_StoredWakeUp";
            this.tab_StoredWakeUp.Padding = new System.Windows.Forms.Padding(3);
            this.tab_StoredWakeUp.Size = new System.Drawing.Size(630, 330);
            this.tab_StoredWakeUp.TabIndex = 1;
            this.tab_StoredWakeUp.Text = "Browse stored computers";
            this.tab_StoredWakeUp.UseVisualStyleBackColor = true;
            // 
            // tabSettings
            // 
            this.tabSettings.BackColor = System.Drawing.Color.Transparent;
            this.tabSettings.Location = new System.Drawing.Point(4, 4);
            this.tabSettings.Name = "tabSettings";
            this.tabSettings.Size = new System.Drawing.Size(630, 330);
            this.tabSettings.TabIndex = 2;
            this.tabSettings.Text = "Settings";
            this.tabSettings.UseVisualStyleBackColor = true;
            // 
            // quick_WakeUp_MAC
            // 
            this.quick_WakeUp_MAC.AllowInternalTab = false;
            this.quick_WakeUp_MAC.AutoHeight = true;
            this.quick_WakeUp_MAC.BackColor = System.Drawing.SystemColors.Window;
            this.quick_WakeUp_MAC.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.quick_WakeUp_MAC.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.quick_WakeUp_MAC.FieldCount = 6;
            this.quick_WakeUp_MAC.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.quick_WakeUp_MAC.Location = new System.Drawing.Point(138, 124);
            this.quick_WakeUp_MAC.Name = "quick_WakeUp_MAC";
            this.quick_WakeUp_MAC.ReadOnly = false;
            this.quick_WakeUp_MAC.Size = new System.Drawing.Size(170, 23);
            this.quick_WakeUp_MAC.TabIndex = 3;
            this.quick_WakeUp_MAC.Text = ":::::";
            // 
            // quick_IP2MAC
            // 
            this.quick_IP2MAC.AllowInternalTab = false;
            this.quick_IP2MAC.AutoHeight = true;
            this.quick_IP2MAC.BackColor = System.Drawing.SystemColors.Window;
            this.quick_IP2MAC.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.quick_IP2MAC.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.quick_IP2MAC.FieldCount = 4;
            this.quick_IP2MAC.Location = new System.Drawing.Point(138, 153);
            this.quick_IP2MAC.Name = "quick_IP2MAC";
            this.quick_IP2MAC.ReadOnly = false;
            this.quick_IP2MAC.Size = new System.Drawing.Size(170, 21);
            this.quick_IP2MAC.TabIndex = 4;
            this.quick_IP2MAC.Text = "...";
            // 
            // quick_IP2MAC_btn
            // 
            this.quick_IP2MAC_btn.DirtyPaletteCounter = 11;
            this.quick_IP2MAC_btn.Location = new System.Drawing.Point(314, 153);
            this.quick_IP2MAC_btn.Name = "quick_IP2MAC_btn";
            this.quick_IP2MAC_btn.Size = new System.Drawing.Size(170, 23);
            this.quick_IP2MAC_btn.TabIndex = 5;
            this.quick_IP2MAC_btn.Text = "Get MAC by IP";
            this.quick_IP2MAC_btn.Values.ExtraText = "";
            this.quick_IP2MAC_btn.Values.Image = null;
            this.quick_IP2MAC_btn.Values.ImageStates.ImageCheckedNormal = null;
            this.quick_IP2MAC_btn.Values.ImageStates.ImageCheckedPressed = null;
            this.quick_IP2MAC_btn.Values.ImageStates.ImageCheckedTracking = null;
            this.quick_IP2MAC_btn.Values.Text = "Get MAC by IP";
            this.quick_IP2MAC_btn.Click += new System.EventHandler(this.quick_IP2MAC_btn_Click);
            // 
            // FrmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(638, 356);
            this.Controls.Add(this.FrmMainTabs);
            this.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.ForeColor = System.Drawing.Color.Black;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "FrmMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "SB Remote WakeUp";
            this.Load += new System.EventHandler(this.FrmMain_Load);
            this.FrmMainTabs.ResumeLayout(false);
            this.tab_QuickWakeUp.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panel_QuickWakeUp.Panel)).EndInit();
            this.panel_QuickWakeUp.Panel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panel_QuickWakeUp)).EndInit();
            this.panel_QuickWakeUp.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.QuickWakeUp_DO_btn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.quick_IP2MAC_btn)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private ComponentFactory.Krypton.Toolkit.KryptonManager kryptonManager1;
        private System.Windows.Forms.TabControl FrmMainTabs;
        private System.Windows.Forms.TabPage tab_QuickWakeUp;
        private System.Windows.Forms.TabPage tab_StoredWakeUp;
        private System.Windows.Forms.TabPage tabSettings;
        private ComponentFactory.Krypton.Toolkit.KryptonHeaderGroup panel_QuickWakeUp;
        private ComponentFactory.Krypton.Toolkit.KryptonButton QuickWakeUp_DO_btn;
        private TestFlexFieldControl.MACAddressControl quick_WakeUp_MAC;
        private ComponentFactory.Krypton.Toolkit.KryptonButton quick_IP2MAC_btn;
        private TestFlexFieldControl.IPAddressControl quick_IP2MAC;
    }
}

