using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using ComponentFactory.Krypton.Toolkit;
using System.Net.Sockets;
using System.Net;
using SQLite.NET;
using System.Globalization;

namespace SB_Remote_WakeUp
{
    //public partial class FrmMain : Form
    public partial class FrmMain : KryptonForm
    {
        private SQLiteClient db;
        private string query;
        private Ini.IniFile cfg = new Ini.IniFile(Application.ExecutablePath.Replace(".exe", ".ini"));


        public FrmMain()
        {
            InitializeComponent();
        }

        private void FrmMain_Load(object sender, EventArgs e)
        {

            cfg.IniWriteValue("GLOBAL", "LastVersion", Application.ProductVersion.ToString());
            OpenSQLiteDB();
            

        }

        private void OpenSQLiteDB()
        {
            bool SQLtableCreated = false;
            try
            {
                db = new SQLiteClient("data.SQLite.db");
            }
            catch (SQLiteException e)
            {
                MessageBox.Show(e.ToString(), "DEBUG - ERROR");
            }

            if (OpenSQLiteDB_CreateTable("WakeUp_computers", "CREATE TABLE 'WakeUp_computers'('id' int(11) NOT NULL, 'ComputerName' char(255) NOT NULL, 'ComputerMAC' char(10) NOT NULL, 'computerGroup' char(32) NULL, PRIMARY KEY ('id'));")) SQLtableCreated = true;
            if (OpenSQLiteDB_CreateTable("WakeUp_groups", "CREATE TABLE 'WakeUp_groups'('id' int(11) NOT NULL, 'GroupName' char(32) NOT NULL, PRIMARY KEY ('id'));")) SQLtableCreated = true;


            if (SQLtableCreated)
            {
                MessageBox.Show("Datab�ze byla �sp�n� zalo�ena.", "SQLite", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }


        }

        private bool OpenSQLiteDB_CreateTable(string TableName, string CreateQuery)
        {
            string table_exists = "";
            bool SQLtableCreated = false;

            table_exists = db.GetOne("SELECT name FROM sqlite_master WHERE type = 'table' AND name = '" + TableName + "'");
            if (table_exists != TableName)
            {
                try
                {
                    db.Execute(CreateQuery);
                    SQLtableCreated = true;
                }
                catch
                {
                    MessageBox.Show("Nepoda�ilo se zalo�it datab�zi.\r\nAplikace nem��e pokra�ovat.", "SQLite", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    Application.Exit();
                }


            }
            return SQLtableCreated;
        }



        // FUNCTIONS
        public class WOLClass : UdpClient
        {
            public WOLClass()
                : base()
            { }
            //this is needed to send broadcast packet

            public void SetClientToBrodcastMode()
            {
                if (this.Active)
                    this.Client.SetSocketOption(SocketOptionLevel.Socket,
                                              SocketOptionName.Broadcast, 0);
            }
        }
        
        private void WakeFunction(string MAC_ADDRESS)
        {
            MAC_ADDRESS =MAC_ADDRESS.Replace(":", "");
            
            WOLClass client = new WOLClass();
            client.Connect(new IPAddress(0xffffffff), 0x2fff); // port=12287 let's use this one 

            client.SetClientToBrodcastMode();
            //set sending bites

            int counter = 0;
            //buffer to be send

            byte[] bytes = new byte[1024];   // more than enough :-)

            //first 6 bytes should be 0xFF

            for (int y = 0; y < 6; y++)
                bytes[counter++] = 0xFF;
            //now repeate MAC 16 times

            for (int y = 0; y < 16; y++)
            {
                int i = 0;
                for (int z = 0; z < 6; z++)
                {
                    bytes[counter++] =
                        byte.Parse(MAC_ADDRESS.Substring(i, 2),
                        NumberStyles.HexNumber);
                    i += 2;
                }
            }

            //now send wake up packet

            int returned_value = client.Send(bytes, 1024);
        }

        private string GetMACfromIP(string IPaddr)
        {
            string MAC = "";
            long MAC_L;
            MAC_L = MacResolver.getRemoteMAC(System.Net.IPAddress.Broadcast.ToString(), IPaddr);
            MAC = MacResolver.formatMac(MAC_L, ':');

            return (MAC);
        }

        // ACTION

        private void QuickWakeUp_DO_btn_Click(object sender, EventArgs e)
        {
            if (quick_WakeUp_MAC.Text.Length == 17)
            {
                WakeFunction(quick_WakeUp_MAC.Text);
                DialogResult res = PSTaskDialog.cTaskDialog.ShowTaskDialogBox("SB Remote WakeUp", "WakeUp command has been sent to remote machine with MAC address " + quick_WakeUp_MAC.Text, "", "", "", "", "", "", PSTaskDialog.eTaskDialogButtons.OK, PSTaskDialog.eSysIcons.Information, PSTaskDialog.eSysIcons.Information);
            }
            else
            {
                DialogResult res = PSTaskDialog.cTaskDialog.ShowTaskDialogBox("SB Remote WakeUp", "Incorect MAC address", "MAC address You have entered is not correct. Please insert correct MAC address and try it again.", "The standard (IEEE 802) format for printing MAC-48 addresses in human-readable media is six groups of two hexadecimal digits, separated by hyphens (:) in transmission order, e.g. 01:23:45:67:89:ab.", "", "", "", "", PSTaskDialog.eTaskDialogButtons.OK, PSTaskDialog.eSysIcons.Error, PSTaskDialog.eSysIcons.Error);
            }
        }

        private void quick_IP2MAC_btn_Click(object sender, EventArgs e)
        {
            quick_WakeUp_MAC.Text = GetMACfromIP(quick_IP2MAC.Text);
            
        }
    }
}