using System;
using System.Windows.Forms;
using FlexFieldControlLib;


namespace TestFlexFieldControl
{
    class IPAddressControl : FlexFieldControl
    {
        public IPAddressControl()
        {
            // the format of this control is 'xxx.xxx.xxx.xxx'
            FieldCount = 4;
            SetMaxLength(3);
            SetSeparatorText(".");
            // except for the first and last separators
            //
            SetSeparatorText(0, String.Empty);
            SetSeparatorText(FieldCount, String.Empty);

            SetValueFormat(ValueFormat.Decimal);
            SetLeadingZeros(false);

            // add ':' key to cede focus for every field
            //
            KeyEventArgs e = new KeyEventArgs(Keys.Oemcomma);
            AddCedeFocusKey(e);

            // this should be the last thing
            //
            Size = MinimumSize;
        }
    }
}
